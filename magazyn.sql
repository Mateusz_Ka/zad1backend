-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Maj 2019, 15:18
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `magazyn`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `czesci_motoryzacyjne`
--

CREATE TABLE `czesci_motoryzacyjne` (
  `id_czesci` int(11) NOT NULL,
  `id_kategorii` int(11) NOT NULL,
  `nazwa` varchar(200) NOT NULL,
  `opis` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `czesci_motoryzacyjne`
--

INSERT INTO `czesci_motoryzacyjne` (`id_czesci`, `id_kategorii`, `nazwa`, `opis`) VALUES
(1, 3, 'Filtr powietrza', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(2, 3, 'Filtr oleju', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(3, 1, 'Opona letnia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(4, 1, 'Opona zimowa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(5, 1, 'Opona wielosezonowa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(6, 2, 'Pióro wycieraczki', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(7, 2, 'Silnik wycieraczek', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(8, 4, 'Tłumik', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(9, 5, 'Tarcze hamulcowe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(10, 5, 'Klocki hamulcowe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.'),
(11, 5, 'Włącznik świateł stop', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus leo, varius sed metus id, porta finibus leo. Vivamus maximus nisl eros, non pharetra leo accumsan vitae. Pellentesque felis elit.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie_czesci`
--

CREATE TABLE `kategorie_czesci` (
  `id_kategorii` int(11) NOT NULL,
  `nazwa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kategorie_czesci`
--

INSERT INTO `kategorie_czesci` (`id_kategorii`, `nazwa`) VALUES
(1, 'Opony'),
(2, 'wycieraczki'),
(3, 'filtry'),
(4, 'wydechy'),
(5, 'hamulce');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `czesci_motoryzacyjne`
--
ALTER TABLE `czesci_motoryzacyjne`
  ADD PRIMARY KEY (`id_czesci`);

--
-- Indexes for table `kategorie_czesci`
--
ALTER TABLE `kategorie_czesci`
  ADD PRIMARY KEY (`id_kategorii`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `czesci_motoryzacyjne`
--
ALTER TABLE `czesci_motoryzacyjne`
  MODIFY `id_czesci` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `kategorie_czesci`
--
ALTER TABLE `kategorie_czesci`
  MODIFY `id_kategorii` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
