const express = require('express');
const mysql = require('mysql');
const app = express();

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'MAGAZYN'
});

function getKategorie() {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM kategorie_czesci`, (error, results, fields) => {
            if (error) { reject(error) };
            resolve(results);
        });
    })
}

function getCzesci(id_kategorii) {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM czesci_motoryzacyjne WHERE id_kategorii=${id_kategorii}`, (error, results, fields) => {
            if (error) { reject(error) };
            resolve(results);
        });
    })
}

app.get('/czesci/:id_kategorii', (req, res) => {
    const id_kategorii = Number(req.params.id_kategorii);
    if (!Number.isSafeInteger(id_kategorii)) {
        res.status(400).send("Błędny parametr");
        return;
    }
    return getCzesci(id_kategorii).then(czesci => res.send(czesci));
})
    
app.get('/kategorie', (req, res) =>{
    console.log()
    getKategorie().then(kategorie => res.send(kategorie))
})

app.listen(3000);